# XLiveLessNess (XLLN)
Games for Windows LiveLessNess. A complete Games For Windows Live (GFWL) rewrite.

# XLLN-Wrappers
These projects are wrappers for existing DLLs (with the possibility for tracing/logging) or may also exist to re-implement it in some cases. These wrappers also serve to invoke all present XLLN-Modules in the case that XLiveLessNess is not installed.

# XLLN-Modules
The purpose of an XLiveLessNess-Module is to enchance some aspect of one or many GFWL Games/Titles or XLiveLessNess itself.

#

## PCCompat.dll
This library returns information about the system hardware and whether particular features are supported (PC Compatibility). This library is old/dated and does not report correct information in modern hardware. Therefore this project aims to re-implement its functionality with the additional ability for manual overrides.

