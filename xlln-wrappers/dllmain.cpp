// dllmain.cpp : Defines the entry point for the DLL application.
#include "dllmain.hpp"
#include "xlln-modules.hpp"

HMODULE xlln_hmod_xlln_wrapper = 0;
HMODULE xlln_hmod_title = 0;

BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
{
	if (ul_reason_for_call == DLL_PROCESS_ATTACH) {
		xlln_hmod_xlln_wrapper = hModule;

		xlln_hmod_title = GetModuleHandle(NULL);
		if (!xlln_hmod_title) {
			return FALSE;
		}

		extern BOOL InitXllnWrapper();
		if (!InitXllnWrapper()) {
			return FALSE;
		}

		// Emphasize that NOTHING else should be done after this point to cause this DLL not to load successfully.
		if (InitXllnModules()) {
			return FALSE;
		}
	}
	else if (ul_reason_for_call == DLL_PROCESS_DETACH) {
		if (UninitXllnModules()) {
			return FALSE;
		}

		extern BOOL UninitXllnWrapper();
		UninitXllnWrapper();
	}
	else if (ul_reason_for_call == DLL_THREAD_ATTACH) {

	}
	else if (ul_reason_for_call == DLL_THREAD_DETACH) {

	}
	return TRUE;
}
