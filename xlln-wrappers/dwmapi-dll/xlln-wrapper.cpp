#include "../dllmain.hpp"
#include "xlln-wrapper.hpp"
#include "../utils/util-hook.hpp"
#include "../utils/utils.hpp"

#ifdef _DEBUG
#define xtra2() __debugbreak();
#else
#define xtra2() ;
#endif
#define xtra() \
			   xtra2()

HINSTANCE hDLL_Proxy = 0;

typedef HRESULT(__stdcall* LPFNDLLFUNC100)(HWND, int, int, int, int, int*, DWORD*, DWORD*);
LPFNDLLFUNC100 lpfnDllFunc100 = 0;
typedef HRESULT(__stdcall* LPFNDLLFUNC102)(UINT);
LPFNDLLFUNC102 lpfnDllFunc102 = 0;
typedef HRESULT(__stdcall* LPFNDLLFUNC142)(HWND, DWORD, PVOID, DWORD);
LPFNDLLFUNC142 lpfnDllFunc142 = 0;
typedef HRESULT(__stdcall* LPFNDLLFUNC144)(BOOL*);
LPFNDLLFUNC144 lpfnDllFunc144 = 0;
typedef HRESULT(__stdcall* LPFNDLLFUNC152)(HWND, DWORD, LPCVOID, DWORD);
LPFNDLLFUNC152 lpfnDllFunc152 = 0;

static bool DwmApiInitOrdinals() {
	lpfnDllFunc100 = (LPFNDLLFUNC100)GetProcAddress(hDLL_Proxy, MAKEINTRESOURCEA(100));
	if (!lpfnDllFunc100)
		return false;
	lpfnDllFunc102 = (LPFNDLLFUNC102)GetProcAddress(hDLL_Proxy, MAKEINTRESOURCEA(102));
	if (!lpfnDllFunc102)
		return false;
	lpfnDllFunc142 = (LPFNDLLFUNC142)GetProcAddress(hDLL_Proxy, MAKEINTRESOURCEA(142));
	if (!lpfnDllFunc142)
		return false;
	lpfnDllFunc144 = (LPFNDLLFUNC144)GetProcAddress(hDLL_Proxy, MAKEINTRESOURCEA(144));
	if (!lpfnDllFunc144)
		return false;
	lpfnDllFunc152 = (LPFNDLLFUNC152)GetProcAddress(hDLL_Proxy, MAKEINTRESOURCEA(152));
	if (!lpfnDllFunc152)
		return false;
	return true;
}

BOOL InitXllnWrapper()
{
#pragma warning(disable: 4996)
	wchar_t* dir_temp = _wgetenv(L"WINDIR");

	wchar_t* dll_path = (wchar_t*)malloc(sizeof(wchar_t) * (wcslen(dir_temp) + 24));
	wsprintf(dll_path, L"%ws\\System32\\dwmapi.dll", dir_temp);

	hDLL_Proxy = LoadLibrary(dll_path);

	free(dll_path);

	if (!hDLL_Proxy) {
		return FALSE;
	}

	if (!DwmApiInitOrdinals()) {
		FreeLibrary(hDLL_Proxy);
		hDLL_Proxy = 0;
		return FALSE;
	}

	return TRUE;
}

BOOL UninitXllnWrapper()
{
	if (hDLL_Proxy) {
		FreeLibrary(hDLL_Proxy);
		hDLL_Proxy = 0;
	}

	return TRUE;
}

// #100
DWMAPI_EXPORT DwmUnknown100(HWND hwnd, int a2, int a3, int a4, int a5, int *a6, DWORD *a7, DWORD *a8) {
	//xtra();
	HRESULT result = lpfnDllFunc100(hwnd, a2, a3, a4, a5, a6, a7, a8);
	return result;
	/*
	*a7 = 0;
	//a8[0] = 0;
	//a8[1] = 0;

	return S_OK;
	return DWM_E_COMPOSITIONDISABLED;
	return DWM_E_ADAPTER_NOT_FOUND;
	return DWM_E_REMOTING_NOT_SUPPORTED;
	return DWM_E_NO_REDIRECTION_SURFACE_AVAILABLE;
	return DWM_E_NOT_QUEUING_PRESENTS;
	return DWM_S_GDI_REDIRECTION_SURFACE;
	return DWM_E_TEXTURE_TOO_LARGE;
	return DWM_S_GDI_REDIRECTION_SURFACE_BLT_VIA_GDI;

	*/
}

// #101
DWMAPI_EXPORT DwmUnknown101(int a1, int a2, int a3, int a4, int a5, int *a6) {
	xtra();
	return S_OK;
}

// #102
DWMAPI_EXPORT DwmEnableComposition(UINT uCompositionAction) {
	xtra();
	HRESULT result = lpfnDllFunc102(uCompositionAction);
	return result;
	return S_OK;
}

// #111
DWMAPI_EXPORT DwmAttachMilContent() {
	xtra();
	return S_OK;
}

// #116
DWMAPI_EXPORT DwmDefWindowProc() {
	xtra();
	return S_OK;
}

// #117
DWMAPI_EXPORT DwmDetachMilContent() {
	xtra();
	return S_OK;
}

// #122
DWMAPI_EXPORT DwmEnableBlurBehindWindow() {
	xtra();
	return S_OK;
}

// #123
DWMAPI_EXPORT DwmEnableMMCSS() {//FIXME
	xtra();
	return S_OK;
}

// #125
DWMAPI_EXPORT DwmUnknown125(int a1, int *a2, int a3) {
	xtra();
	return S_OK;
}

// #126
DWMAPI_EXPORT DwmUnknown126(char a1, int a2) {
	xtra();
	return S_OK;
}

// #128
DWMAPI_EXPORT DwmUnknown128(DWORD *a1) {
	xtra();
	return S_OK;
}

// #129
DWMAPI_EXPORT DwmUnknown129(int a1) {
	xtra();
	return S_OK;
}

// #130
DWMAPI_EXPORT DwmUnknown130() {
	xtra();
	return S_OK;
}

// #135
DWMAPI_EXPORT DwmExtendFrameIntoClientArea() {
	xtra();
	return S_OK;
}

// #136
DWMAPI_EXPORT DwmFlush() {
	xtra();
	return S_OK;
}

// #137
DWMAPI_EXPORT DwmGetColorizationColor() {
	xtra();
	return S_OK;
}

// #138
DWMAPI_EXPORT DwmGetCompositionTimingInfo() {
	xtra();
	return S_OK;
}

// #139
DWMAPI_EXPORT DwmGetGraphicsStreamClient() {
	xtra();
	return S_OK;
}

// #140
DWMAPI_EXPORT DwmGetGraphicsStreamTransformHint() {
	xtra();
	return S_OK;
}

// #141
DWMAPI_EXPORT DwmGetTransportAttributes() {
	xtra();
	return S_OK;
}

// #142
DWMAPI_EXPORT DwmGetWindowAttribute(HWND hwnd, DWORD dwAttribute, PVOID pvAttribute, DWORD cbAttribute) {
	xtra();
	//DwmWindowAttribute dwAttribute;
	//memset(pvAttribute, 0, cbAttribute);
	HRESULT result = lpfnDllFunc142(hwnd, dwAttribute, pvAttribute, cbAttribute);
	return result;
	return S_OK;
}

// #143
DWMAPI_EXPORT DwmInvalidateIconicBitmaps() {
	xtra();
	return S_OK;
}

// #144
DWMAPI_EXPORT DwmIsCompositionEnabled(BOOL* pfEnabled) {
	xtra();
	//*pfEnabled = true;
	HRESULT result = lpfnDllFunc144(pfEnabled);
	return result;
	return S_OK;
}

// #145
DWMAPI_EXPORT DwmModifyPreviousDxFrameDuration() {
	xtra();
	return S_OK;
}

// #146
DWMAPI_EXPORT DwmQueryThumbnailSourceSize() {
	xtra();
	return S_OK;
}

// #147
DWMAPI_EXPORT DwmRegisterThumbnail() {
	xtra();
	return S_OK;
}

// #148
DWMAPI_EXPORT DwmSetDxFrameDuration() {
	xtra();
	return S_OK;
}

// #149
DWMAPI_EXPORT DwmSetIconicLivePreviewBitmap() {
	xtra();
	return S_OK;
}

// #150
DWMAPI_EXPORT DwmSetIconicThumbnail() {
	xtra();
	return S_OK;
}

// #151
DWMAPI_EXPORT DwmSetPresentParameters() {
	xtra();
	return S_OK;
}

// #152
DWMAPI_EXPORT DwmSetWindowAttribute(HWND hwnd, DWORD dwAttribute, LPCVOID pvAttribute, DWORD cbAttribute) {
	xtra();
	HRESULT result = lpfnDllFunc152(hwnd, dwAttribute, pvAttribute, cbAttribute);
	return result;
	return S_OK;
}

// #153
DWMAPI_EXPORT DwmUnregisterThumbnail() {
	xtra();
	return S_OK;
}

// #154
DWMAPI_EXPORT DwmUpdateThumbnailProperties() {
	xtra();
	return S_OK;
}
