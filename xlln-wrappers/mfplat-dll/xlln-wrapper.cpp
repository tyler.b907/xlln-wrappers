#include "../dllmain.hpp"
#include "xlln-wrapper.hpp"
#include "../utils/util-hook.hpp"
#include "../utils/utils.hpp"

#ifdef _DEBUG
#define xtra2() __debugbreak();
#else
#define xtra2() ;
#endif
#define xtra() \
			   xtra2()

#define MF_E_DISABLED_IN_SAFEMODE 0xC00D36EF
#define MF_E_BAD_STARTUP_VERSION 0xC00D36E3

BOOL InitXllnWrapper()
{

	return TRUE;
}

BOOL UninitXllnWrapper()
{

	return TRUE;
}

MFPlat_EXPORT MFStartup(ULONG Version, DWORD dwFlags) {
	xtra();

	if ((Version & 0xFFFF0000) > 0x20000)
		return MF_E_BAD_STARTUP_VERSION;

	if (GetSystemMetrics(SM_CLEANBOOT) > 0)
		return MF_E_DISABLED_IN_SAFEMODE;

	//return S_OK;
	return E_OUTOFMEMORY;
}

MFPlat_EXPORT MFShutdown() {
	xtra();
	return S_OK;
}
