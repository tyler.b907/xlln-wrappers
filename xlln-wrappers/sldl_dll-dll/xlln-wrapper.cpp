#include "../dllmain.hpp"
#include "xlln-wrapper.hpp"
#include "../utils/util-hook.hpp"
#include "../utils/utils.hpp"

#ifdef _DEBUG
#define xtra2() __debugbreak();
#else
#define xtra2() ;
#endif
#define xtra() \
			   xtra2()

BOOL InitXllnWrapper()
{

	return TRUE;
}

BOOL UninitXllnWrapper()
{

	return TRUE;
}

SLDL_EXPORT SLDLClose(int a1) {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLConsumeRight(int a1, char* a2, int a3, int a4, int a5) {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLDepositOfflineConfirmationId() {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLGenerateOfflineInstallationId() {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLGetInformation() {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLGetInformationDWORD() {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLGetLicense() {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLGetLicenseFileId() {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLGetLicenseInformation() {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLGetLicensingStatusInformation(int a1, int a2, int a3, int a4, int a5, int a6) {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLGetPKeyId() {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLGetPolicyInformation() {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLGetPolicyInformationDWORD() {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLGetProductSkuInformation() {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLGetSLIDList() {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLInitialize() {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLInstallLicense() {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLInstallProofOfPurchase() {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLOpen(int a1) {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLRegisterLoggingCallback() {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLShutdown() {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLUninstallLicense() {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLUninstallLicensesByApplicationID() {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLUninstallProofOfPurchase() {
	xtra();
	return S_OK;
}

SLDL_EXPORT SLDLUnregisterLoggingCallback() {
	xtra();
	return S_OK;
}
